import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent {

  @Input() nome: string;
  @Output() clickDoButton: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  onClickButton(nome: string): void {
    this.clickDoButton.emit(nome);
  }

}
