import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  buttonTitle = 'Botão dois';


  onClick(value: string): void {
    console.log('teste', value);
  }
}
